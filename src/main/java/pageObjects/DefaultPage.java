package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class DefaultPage {
	
	public WebDriver driver;
	
	public DefaultPage(WebDriver driver) {
		this.driver = driver;
	}
	
//===============WebElements=======================
	
	By selectsociety = By.id("DDL_SocietyNew");
	 
	
//==============Methods============================
	
	public WebElement selectSociety() {
		return driver.findElement(selectsociety);
	}
	
	public String getTitle() {
		return driver.getTitle();
	}
	
	
}
