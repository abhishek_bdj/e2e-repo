package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginPage {
	
	public WebDriver driver;
	
	public LoginPage(WebDriver driver) {
		this.driver = driver; 
	}
	
//==============WebElements==============================//	
	
	By username = By.id("txt_username");
	By password = By.id("txt_password");
	By signin = By.id("btnSignIn");
	
	
		
//==============Methods==================================//	

	public WebElement Username() {
		return driver.findElement(username);
	}
	
	public WebElement Password() {
		return driver.findElement(password);
	}
	
	public DefaultPage SignIn() {
		driver.findElement(signin).click();
		DefaultPage def = new DefaultPage(driver);
		return def;
		//return new DefaultPage(driver);
	}
	
	
}
