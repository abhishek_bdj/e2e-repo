package Learning;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import resources.BaseClass;
import resources.ExtentReporterNg;

public class Listeners extends BaseClass implements ITestListener {
	ExtentReports extent = ExtentReporterNg.getReportObject();
	ExtentTest test;
	ThreadLocal<ExtentTest> extentTest = new ThreadLocal<ExtentTest>();	//to execute the test parallel in thread safe manner.
	@Override
	public void onTestStart(ITestResult result) {
		// TODO Auto-generated method stub
		test = extent.createTest(result.getMethod().getMethodName());		//will retrieve the test method name.
		extentTest.set(test);			//set the test to extentTest object.
		
	}

	@Override
	public void onTestSuccess(ITestResult result) {
		// TODO Auto-generated method stub
		extentTest.get().log(Status.PASS, "Test Pass");			//replace all test with extentTest.get()
	}

	@Override
	public void onTestFailure(ITestResult result) { // To execute this code when the test fails.
		// TODO Auto-generated method stub
		extentTest.get().fail(result.getThrowable());	//failure og will be retrieved and sent here.
		WebDriver driver = null;
		String testmethodname = result.getMethod().getMethodName(); // to fetch the method name, result parameter will
		// methodname.
		// Screenshot in testFailure.
		try {
			driver = (WebDriver)result.getTestClass().getRealClass().getDeclaredField("driver")
					.get(result.getInstance()); // to get the driver field from testcase method.
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			extentTest.get().addScreenCaptureFromPath(getScreenShotPath(testmethodname, driver), result.getMethod().getMethodName());
			//getScreenShotPath(testmethodname, driver);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void onTestSkipped(ITestResult result) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTestFailedWithTimeout(ITestResult result) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onStart(ITestContext context) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onFinish(ITestContext context) {
		// TODO Auto-generated method stub
		extent.flush();
	}

}
