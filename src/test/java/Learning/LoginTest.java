package Learning;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pageObjects.DefaultPage;
import pageObjects.LoginPage;
import resources.BaseClass;

public class LoginTest extends BaseClass {
	public WebDriver driver;
	// need to initialixe this log object in all the test cases to use logging.
	// also add the resources tag in <build> tag to join the log4j to maven.
	public static Logger log = LogManager.getLogger(BaseClass.class.getName());

	// WebDriver driver; //To user different driver on every execution cycle.
	// Without declaring driver here same copy of drive is overridden many times to
	// complete the test.
	@BeforeTest
	public void setup() throws IOException {
		driver = initializeDriver();
		log.info("Driver is initialized");
	}

	@Test(dataProvider = "getData")
	public void testLogin(String username, String password) throws IOException {
		// the parameters are as per the getData() will return.
		driver.get(prop.getProperty("url")); // url is fetched from data.properties file.
		log.info("Navigated to LoginPage");
		LoginPage lp = new LoginPage(driver);
		lp.Username().sendKeys(username);
		lp.Password().sendKeys(password);
		DefaultPage def = lp.SignIn(); // object is created in signIn(), and pass Next landing page object.
		log.info("Signed In");

		// DefaultPage def = new DefaultPage(driver); //Instantiated in last step.
		// System.out.println(def.getTitle());

		// Compare the text from browser title with actual text.
		Assert.assertEquals(def.getTitle(), "DashBoard"); // Compare actual with expected
		Assert.assertTrue(def.selectSociety().isDisplayed()); // It expects always to be true. Pass if displayed.
		// Assert.assertFalse(def.selectSociety().isDisplayed()); //It expects always to
		// be false. Fail if displayed.

	}

	@DataProvider
	public Object[][] getData() {

		// Row stands for how many different data types test must run. eg: login
		// restricted users and non-restricted user.
		// Column stands for how many values must be passed for each test.
		Object[][] data = new Object[2][2];

		// 0th row
		data[0][0] = "viral@abc.com";
		data[0][1] = "Pass@123";
		// data[0][2] = "Restricted";

		// 1st row
		data[1][0] = "santosh@hukamboss.com";
		data[1][1] = "Pass@1234==";
		// data[1][2] = "Non Restricted";

		return data;

	}

	@AfterTest
	public void tearDown() {
		driver.close();
	}

}
