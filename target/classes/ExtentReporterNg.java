package resources;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;

public class ExtentReporterNg {
	static ExtentReports extent;
	public static ExtentReports getReportObject() {
		// ExtentReports, ExtentSparkReporter		//Two most important classes used in ExtentReports.
		
		String path = System.getProperty("user.dir") + "\\reports\\index.html";		//The path where the reports will be created.
		ExtentSparkReporter reporter = new ExtentSparkReporter(path);
		reporter.config().setReportName("Web Automation Results"); // To apply report name
		reporter.config().setDocumentTitle("Test Results"); // To apply test document name
		
		extent = new ExtentReports();
		extent.attachReporter(reporter);
		extent.setSystemInfo("Tester", "Abhishek");
		
		return extent;
	}
	

}
